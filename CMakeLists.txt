SET(CMAKE_INCLUDE_CURRENT_DIR ON)
include_directories( include ${Boost_INCLUDE_DIRS} .)

set(MY_CMAKE_WARNING_FLAG  " -Wall -Wextra -Wno-unused-parameter")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  ${MY_CMAKE_WARNING_FLAG}")
set(CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS}  ${MY_CMAKE_WARNING_FLAG}")

set(Boost_LIBRARY_DIR /usr/local/lib)
add_subdirectory(util)
add_subdirectory(common)
