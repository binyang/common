#ifndef __ASIOFUNC_H
#define __ASIOFUNC_H

#include <boost/asio.hpp>
#include <string>

// const char * ip_ntoa(uint32_t IP, char * pBuf, int len);
// const char * ip_ntostr(uint32_t IP, uint16_t port, char * pBuf, int len);
uint32_t Ip2Int(const char *ip);
int getRemoteEndpoint(const boost::asio::ip::tcp::socket &socket_, uint32_t &ip, uint16_t &port);
const char *getRemoteEndpointS(const boost::asio::ip::tcp::socket &socket_, std::string & addr);

uint64_t endpoint2Id(uint32_t ip, uint16_t port);
void id2Endpoint(uint64_t id, uint32_t &ip, uint16_t &port);

long long getNowMsecs();
int getNowTmStr(std::string & tm);

int splitIpPort(const std::string &addr, std::string &ip, std::string &port);
int addr2IpPort(const std::string &addr, uint32_t &uip, uint16_t &uport);
int addr2EpId(const std::string &addr, uint64_t &id);

std::vector<uint8_t> xorEnDecrypt(const std::vector<uint8_t>& input);
std::string base64_encode(uint8_t const* buf, unsigned int bufLen);
std::vector<uint8_t> base64_decode(uint8_t const* buf, unsigned int bufLen);
std::string url_encode(const std::string &value);
std::string url_decode(const std::string &SRC);
std::string stringTrim(const std::string & str);
#endif