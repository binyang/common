#include "asiofunc.h"
#include <string.h>
#include <iostream>
#include <chrono>

namespace bai = boost::asio::ip;

uint64_t endpoint2Id(uint32_t ip, uint16_t port)
{
    uint64_t  ret = ip;
    return ret << 16 | port;
}

void id2Endpoint(uint64_t id, uint32_t &ip, uint16_t &port)
{
    port = id & 0xFFFF;
    ip = uint32_t(id >> 16);
}

uint32_t Ip2Int(const char *ip)
{
    return bai::address_v4::from_string(ip).to_ulong();
}

int getRemoteEndpoint(const bai::tcp::socket &socket_, uint32_t &ip, uint16_t &port)
{
    try
    {
        bai::tcp::endpoint epPeer = socket_.remote_endpoint();
        ip = ::ntohl(epPeer.address().to_v4().to_ulong());
        port = epPeer.port();
        
//         char pTmp[64];
//         BOOST_LOG_TRIVIAL(debug) << "getRemoteEndpoint:" <<  ip_ntostr(ip, port, pTmp, 64) << std::endl;
        return 0;
    }
    catch (std::exception& e)
    {
        std::cerr << "getRemoteEndpoint exception: " << e.what() << "\n";
    }
    return -1;
}

const char *getRemoteEndpointS(const bai::tcp::socket &socket_, std::string & addr)
{
    if (socket_.is_open())
    {
        bai::tcp::endpoint epPeer = socket_.remote_endpoint();
        char pTmp[64];
        snprintf(pTmp, sizeof(pTmp), "%s:%d", epPeer.address().to_string().c_str(), epPeer.port()); 
        addr = pTmp;
        return addr.c_str();
    }
    return NULL;
}

long long getNowMsecs()
{
    return std::chrono::system_clock::now().time_since_epoch() / 
    std::chrono::milliseconds(1);
}

int getNowTmStr(std::string &tm)
{
    const boost::posix_time::ptime now = 
        boost::posix_time::microsec_clock::local_time();
    const boost::posix_time::time_duration td = now.time_of_day();

    const long milliseconds = td.total_milliseconds() -
                              ((td.hours() * 3600 + td.minutes() * 60 + td.seconds()) * 1000);
    char buf[64];
    sprintf(buf, "%02ld:%02ld:%02ld.%03ld", 
        td.hours(), td.minutes(), td.seconds(), milliseconds);
    tm = buf;
    return 0;
}

int splitAddr(const std::string &addr, std::string &ip, std::string &port)
{
    const char *ptr = strchr(addr.c_str(), ':');
    int len;
    if ( ptr != NULL)
    {
        len     = int(ptr - addr.c_str());
        ip      = addr.substr(0, len);
        port    = addr.substr(len+1);
        return 0;
    }
    else
    {
        ip      = addr;
        port    ="0";
        return -1;
    }
}

int addr2IpPort(const std::string &addr, uint32_t &uip, uint16_t &uport)
{
    std::string ip, port;
    if ( !splitAddr(addr, ip, port))
    {
        uip = ::ntohl(bai::address_v4::from_string(ip).to_ulong());
        uport = atoi(port.c_str());
        return 0;
    }
    return -1;
}

int addr2EpId(const std::string &addr, uint64_t &id)
{
    uint32_t ip;
    uint16_t port;
    if ( !addr2IpPort(addr, ip, port))
    {
        id  = endpoint2Id(ip, port);
        return 0;
    }
    return -1;
}

static const std::string base64_chars = 
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";


static inline bool is_base64(uint8_t c) 
{
  return (isalnum(c) || (c == '+') || (c == '/'));
}

std::string base64_encode(uint8_t const* buf, unsigned int bufLen) 
{
  std::string ret;
  int i = 0;
  int j = 0;
  uint8_t char_array_3[3];
  uint8_t char_array_4[4];

  while (bufLen--) 
  {
    char_array_3[i++] = *(buf++);
    if (i == 3) 
    {
      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
      char_array_4[3] = char_array_3[2] & 0x3f;

      for(i = 0; (i <4) ; i++)
        ret += base64_chars[char_array_4[i]];
      i = 0;
    }
  }

  if (i)
  {
    for(j = i; j < 3; j++)
      char_array_3[j] = '\0';

    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
    char_array_4[3] = char_array_3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      ret += base64_chars[char_array_4[j]];

    while((i++ < 3))
      ret += '=';
  }

  return ret;
}

std::vector<uint8_t> base64_decode(uint8_t const *encoded_string, unsigned int  in_len) 
{
  int i = 0;
  int j = 0;
  int in_ = 0;
  uint8_t char_array_4[4], char_array_3[3];
  std::vector<uint8_t> ret;

  while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_]))
  {
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i ==4)
    {
      for (i = 0; i <4; i++)
        char_array_4[i] = base64_chars.find(char_array_4[i]);

      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
          ret.push_back(char_array_3[i]);
      i = 0;
    }
  }

  if (i) 
  {
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = base64_chars.find(char_array_4[j]);

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
  }

  return ret;
}

static std::vector<uint8_t> xorKey = {0x55, 0xae, 0x8c, 0x14};
std::vector<uint8_t> xorEnDecrypt(const std::vector<uint8_t>& input)
{
    std::vector<uint8_t> ret;
    ret.reserve(input.size());

    std::vector<uint8_t>::const_iterator keyIterator = xorKey.begin();
    std::vector<uint8_t>::const_iterator inputIterator;
    for(inputIterator = input.begin(); inputIterator != input.end(); ++ inputIterator)
    {
        ret.push_back(*inputIterator ^ *keyIterator);
        if(++ keyIterator == xorKey.end())
            keyIterator = xorKey.begin();
    }
    return ret;
}

#include <cctype>
#include <iomanip>
#include <sstream>

std::string url_encode(const std::string &value)
{
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for (std::string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
        std::string::value_type c = (*i);

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << std::uppercase;
        escaped << '%' << std::setw(2) << int((unsigned char) c);
        escaped << std::nouppercase;
    }

    return escaped.str();
}

std::string url_decode(const std::string &SRC)
{
    std::string ret;
    char ch;
    int i, ii;
    for (i=0; i<SRC.length(); i++) {
        if (int(SRC[i])==37) {
            sscanf(SRC.substr(i+1,2).c_str(), "%x", &ii);
            ch=static_cast<char>(ii);
            ret+=ch;
            i=i+2;
        } else {
            ret+=SRC[i];
        }
    }
    return (ret);
}

std::string stringTrim(const std::string& str)
{
    size_t first = str.find_first_not_of(' ');
    if (std::string::npos == first)
    {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}
