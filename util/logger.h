#ifndef _LOGGER_HPP
#define _LOGGER_HPP
#include <boost/log/trivial.hpp>
#include <boost/log/core.hpp>
#include <string>
void initLog(const std::string & logFile, const std::string & logLevel);

#endif