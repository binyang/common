#include "logger.h"

#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup.hpp>
static int logLevel2Int(const std::string & logLevel)
{
    std::vector<std::string> levels = {"trace", "debug", "info", "warning", "error", "fatal"};
    for (uint32_t i = 0; i < levels.size(); ++i)
    {
        if (levels[i] == logLevel)
        {
            return i;
        }
    }
    return 0;
}

//static boost::log::sources::severity_logger< boost::log::trivial::severity_level > lg;

void initLog(const std::string & logFile, const std::string & logLevel)
{
    boost::log::add_file_log
    (
        boost::log::keywords::file_name = logFile  + "_%N.log", 
        boost::log::keywords::rotation_size = 1 * 1024 * 1024 * 10,
        boost::log::keywords::max_size = 20 * 1024 * 1024,
        boost::log::keywords::time_based_rotation = boost::log::sinks::file::rotation_at_time_point(0, 0, 0),
        boost::log::keywords::format = "[%TimeStamp%]: %Message%",
        boost::log::keywords::auto_flush = true
    );
    boost::log::add_common_attributes();
    boost::log::core::get()->set_filter
    (
        boost::log::trivial::severity >= logLevel2Int(logLevel)
    );
//     BOOST_LOG_SEV(lg, trace) << "A trace severity message";
}

