#ifndef __TQUEUE_H
#define __TQUEUE_H

#include <deque>
#include <assert.h>
#include <inttypes.h>

class TQueue
{
public:
    int push_back(char *pArray, uint32_t bytes)
    { 
        m_data.insert(m_data.end(), pArray, pArray + bytes);
        return 0;
    }
    
    int pop_front(char *pBuf, uint32_t bytes)
    { 
        assert(bytes > 0);
        if (bytes > m_data.size())
            bytes = (uint32_t) m_data.size();
        
        std::copy(m_data.begin(), m_data.begin() + bytes, pBuf);
        m_data.erase(m_data.begin(), m_data.begin() + bytes);
        return bytes;
    }
    int size() const
    {
        return (int)m_data.size();
    }
private:    
      std::deque<char> m_data;
};

#endif