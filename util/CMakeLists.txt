SET(util_SRCS
    logger.cpp
    crc16.cpp
    asiofunc.cpp
)
add_definitions(-DBOOST_LOG_DYN_LINK )     
add_library(util STATIC ${util_SRCS})
