
#include "gzipstream.h"
#include <assert.h>
#include <string.h>
#define Z_INIT_ERROR            (-10)

GzipStream::GzipStream (int mode, ByteBuf_t* pLBuf, int level)
    : m_errno(0)
    , m_zLevel(level)
    , m_zipMode(mode)
    , m_pByteBuf(pLBuf)     

{
    memset(&m_zstrm, 0x0, sizeof(m_zstrm));
    m_zstrm.zalloc = Z_NULL;
    m_zstrm.zfree = Z_NULL;
    m_zstrm.opaque = Z_NULL;
    m_zstrm.state = NULL;
    if (m_zipMode == ZIP_DEF) 
        initDef();
    else 
        initInf();
}


GzipStream::~GzipStream()
{
    release();
}


int GzipStream::release()
{
    if (m_zipMode == ZIP_DEF)
        return deflateEnd(&m_zstrm);
    else
        return inflateEnd(&m_zstrm);
}

int GzipStream::initDef()
{
    int ret;
    
    if (m_zipMode != ZIP_DEF)
        return Z_INIT_ERROR;
    
    if (!m_zstrm.state)
        ret = deflateInit(&m_zstrm, m_zLevel);
    else
        ret = deflateReset(&m_zstrm);
    
    if (ret != Z_OK)
        return Z_INIT_ERROR;
    return 0;   
}

int GzipStream::initInf()
{
    int ret;
    if (m_zipMode != ZIP_INF)
        return Z_INIT_ERROR;
    
    if (!m_zstrm.state)
        ret = inflateInit(&m_zstrm);
    else
        ret = inflateReset(&m_zstrm);    
    
    if (ret != Z_OK)
        return Z_INIT_ERROR;
    return 0;    
}

//note: write data from inbuf's offset 
int GzipStream::compress (const char *pInBuf, int dataLen, int offset)
{
    int ret, addLen;
    Bytef *pCurrTail;

    addLen = 0;
    m_zstrm.avail_in    = dataLen;
    m_zstrm.next_in     = (Bytef*)(pInBuf);
    do 
    {
        m_zstrm.avail_out = 2048;
        pCurrTail = (Bytef *)(m_pByteBuf->data() + offset + addLen);
        m_zstrm.next_out = pCurrTail;
        
        ret = deflate(&m_zstrm, Z_SYNC_FLUSH);
        assert(ret != Z_STREAM_ERROR);
        
        addLen += (int)(m_zstrm.next_out - pCurrTail);
    } 
    while (m_zstrm.avail_in > 0);
    return addLen;
}

int GzipStream::uncompress (const char *pInBuf, int dataLen)
{
    int ret, addLen;
    Bytef * pCurrTail;
    
    addLen = 0;
    //m_pByteBuf->resize(FTPBUF_DEFSIZE + 512); //void memory reallocation
    m_zstrm.avail_in = dataLen;
    m_zstrm.next_in = (Bytef*)(pInBuf);
    do 
    {
        m_zstrm.avail_out = 2048;
        pCurrTail = (Bytef *)(m_pByteBuf->data() + addLen);
        m_zstrm.next_out = pCurrTail;
        
        ret = inflate (&m_zstrm, Z_SYNC_FLUSH);
        assert(ret != Z_STREAM_ERROR);
        switch (ret)
        {
        case Z_NEED_DICT:
            ret = Z_DATA_ERROR;
            return ret;
        case Z_DATA_ERROR:
        case Z_MEM_ERROR:
             return ret;
        }
        
        addLen += (int)(m_zstrm.next_out - pCurrTail);
    } 
    while (m_zstrm.avail_in > 0);
    return addLen;
}

int GzipStream::getZipErr(int errno, std::string &errMsg)
{   
    switch (errno)
    {
        case Z_ERRNO:
            errMsg = "error reading or writing";
            return Z_ERRNO;
        case Z_STREAM_ERROR:
            errMsg = "invalid compression level";
            return Z_STREAM_ERROR;
        case Z_DATA_ERROR:
            errMsg = "invalid or incomplete deflate data";
            return Z_DATA_ERROR;
        case Z_MEM_ERROR:
            errMsg = "out of memory";
            return Z_MEM_ERROR;
        case Z_VERSION_ERROR:
            errMsg = "zlib version mismatch";
            return Z_VERSION_ERROR;
        case Z_INIT_ERROR:
            errMsg = "init failed";
            return Z_INIT_ERROR;
        default:
            errMsg = "unknown error";    
            return -100;
    }
}   

