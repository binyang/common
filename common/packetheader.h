#ifndef __PACKETHEADER__H
#define __PACKETHEADER__H

#include <inttypes.h>
#include <boost/concept_check.hpp>
#include <util/crc16.h>

enum
{
    RT_AUTH             = 1,                          
    RT_FILE_FTP,
    RT_THIN,
    RT_SVR_CMD,
    RT_WEB_CMD
};

#define AUTH_PASSED             (1<<0)
#define ZIP_COMPRESSED          (1<<1)


class PacketHeader
{
public:
    PacketHeader();
    void init();
    void buildHeader ( uint8_t mType, uint8_t sType, uint16_t reserved, uint32_t totLen);
    bool unpack ( const char * pBuf, int bufLen );

    void setMajorType (uint8_t mType )
    {   m_mType = mType;        }
   
    uint8_t getMajorType() const
    {   return m_mType;         }
    
    void setMinorType (uint8_t sType )
    {   m_sType = sType;        }
   
    uint8_t getMinorType() const
    {   return m_sType;         }
    
    uint16_t getReserved() const
    {   return m_reserved;      }
    
    void setReserved ( uint16_t reserved )
    {   m_reserved = reserved;  }
    
    uint32_t getBodyLen() const
    {   return m_totLen - size();       }
    
    uint32_t getPackLen() const
    {   return m_totLen;       }
    
    void setPackLen ( uint32_t PackLen )
    {
        m_totLen = PackLen;
        calCRC16();
    }
    
    static int size()
    {   return sizeof(PacketHeader);    }

    bool isValid();
    
private:
    uint16_t calCRC16();
    
private:
    uint16_t    m_crc;
    uint8_t     m_mType; //msg
    uint8_t     m_sType; //sub msg
    uint16_t    m_reserved;
    uint32_t    m_totLen;
};


#endif 