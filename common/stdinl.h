#ifndef __STDINL__H
#define __STDINL__H

#include <vector>
#include <string>
#include <utility>
#include <functional>
#define TMPEXT          ".tmp~"
//#define FTPBUF_DEFSIZE  32768
#define FTPBUF_DEFSIZE  102400

//packet header resvered flag
#define BIT_ERROR       (1<<0)
#define BIT_AUTHED      (1<<1)
#define BIT_ZIP         (1<<2)

typedef std::function< void (void *args1, void *args2) > cb_fn2;
typedef std::vector<std::string> StrVector_t;
typedef std::vector< std::vector< std::string > >        StrVVector_t;
typedef std::vector<char> ByteBuf_t;

enum LinkType
{
    LINK_UP = 1,
    LINK_DOWN
};

enum AgentMode
{
    R_MASTER = 1,
    R_SLAVE
};

enum FsChge
{ 
    FILE_ADD = 0, 
    FILE_DEL, 
    FILE_RNE
}; 

typedef std::vector<std::pair<std::string, std::string>> Dir2IdArr_t;


#endif 