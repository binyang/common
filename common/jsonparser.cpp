#include "jsonparser.h"
#include <common/fsfunc.h>
#include <util/logger.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <iostream>

namespace bp = boost::property_tree;

int JsonParser::trJson2Map(const std::string &inStr, JsonMap_t &jsonMap)
{
    bp::ptree pt;
    //bp::read_json(m_confFile, pt);
    
    std::istringstream is (inStr);
    bp::read_json (is, pt);
  
    for (bp::ptree::const_iterator it1 = pt.begin(); it1 != pt.end(); ++it1)
    {
        std::map<std::string, std::string> subMap;
        for (bp::ptree::const_iterator it2 = it1->second.begin(); it2 != it1->second.end(); ++it2)
        {
            subMap[it2->first] = it2->second.get_value<std::string>();
        }
        jsonMap [ it1->first ] = subMap;
    }
    return 0;
}

int JsonParser::trMap2Json(const JsonMap_t &jsonMap, std::string &outStr)
{
    bp::ptree pt; 
    for (auto& entry: jsonMap)
    {
        bp::ptree subPt; 
        for (auto& entry2: entry.second)
        {     
            subPt.put (entry2.first, entry2.second);
        }
        pt.put_child(entry.first, subPt);
    }
    
    std::ostringstream oss; 
    bp::write_json (oss, pt, false);
    outStr = oss.str();
    return 0;
}
