#include "clntinfo.h"
#include <util/logger.h>

#include <memory.h>
#include <iostream>
#include <time.h>

    
ClntInfo::ClntInfo()
{
    m_startTm = (uint32_t)time(NULL);
    m_installTm = (uint32_t)time(NULL);
    
    m_clntVer = "1.0801";
//     m_hostName = "office backup server"; 
//     m_osName = "windows";
//     m_osVer = "10 x";
//     m_arch = "64bit";

}

ClntInfo::~ClntInfo()
{

}

void ClntInfo::setNativeVars(std::string & hostName, std::string &osName, std::string &osVer,  std::string &arch)
{
    m_hostName  = hostName; 
    m_osName    = osName;
    m_osVer     = osVer;
    m_arch      = arch;   
}

void ClntInfo::debug()
{
    BOOST_LOG_TRIVIAL(debug) << 
    "\nm_startTm:"<<m_startTm <<
    "\nm_installTm:"<<m_installTm <<
    
    "\nm_clntVer:"<<m_clntVer <<
    "\nm_hostName:"<<m_hostName <<
    "\nm_osName:"<<m_osName <<    
    "\nm_arch:"<<m_arch << std::endl;
}

