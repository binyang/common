SET(common_SRCS
    proto.cpp
    jsonparser.cpp
    conf.cpp
    packetheader.cpp
    packer.cpp
    gzipstream.cpp
    clntinfo.cpp
    authproto.cpp
    ftpproto.cpp
    thinproto.cpp
    fsfunc.cpp
    #svrcmdproto.cpp
)
add_definitions(-DBOOST_LOG_DYN_LINK )     
add_library(common STATIC ${common_SRCS})
