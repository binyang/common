
#include "thinproto.h"
#include <common/packetheader.h>
#include <common/packer.h>
#include <util/asiofunc.h>
#include "proto.h"
#include <memory.h>
#include <iostream>
#include <assert.h>
#include <sys/stat.h>


int ThinProto::encode_THIN_LIVE(char* pOutBuf, int size, uint8_t liveType, uint16_t reserved, uint64_t tm, const std::string& cmds)
{
    int tot = PacketHeader::size();
    char *ptr = pOutBuf + PacketHeader::size();
    
    int len = Packer::pack(ptr, size, tm);                              // tm
    tot += len;
    ptr += len;
    if (cmds.size() > 0)
    {
        memcpy(ptr, cmds.c_str(), cmds.size());     //careful: to be efificent, just copy buffer without encoding
        tot += cmds.size();
    }
    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(RT_THIN, liveType, reserved, tot);
    
    return tot;
}

int ThinProto::encode_THIN_CLNT_INFO(char *pOutBuf, int size, const ClntInfo &clntInfo)
{
    int tot = PacketHeader::size();
    char *ptr = pOutBuf + PacketHeader::size();
    
    int len = Packer::pack(ptr, 1024, clntInfo.getStartTm()); // startTm
    tot += len;
    ptr += len;
    
    len = Packer::pack(ptr, 1024, clntInfo.getInstallTm()); // installTm
    tot += len;
    ptr += len;

    len = Packer::packLenStr(ptr, 1024, clntInfo.getClntVer().c_str(), int(clntInfo.getClntVer().size()));
    tot += len;
    ptr += len;
    
    len = Packer::packLenStr(ptr, 1024, clntInfo.getHostName().c_str(), (int)clntInfo.getHostName().size());
    tot += len;
    ptr += len;
    
    len = Packer::packLenStr(ptr, 1024, clntInfo.getOsName().c_str(), (int)clntInfo.getOsName().size());
    tot += len;
    ptr += len;

    len = Packer::packLenStr(ptr, 1024, clntInfo.getOsVer().c_str(), (int)clntInfo.getOsVer().size());
    tot += len;
    ptr += len;

    len = Packer::packLenStr(ptr, 1024, clntInfo.getArch().c_str(), (int)clntInfo.getArch().size());
    tot += len;
    ptr += len;

    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(RT_THIN, THIN_CLNT_INFO, 0, tot);
    
    return tot;
}

int ThinProto::decode_THIN_CLNT_INFO(char *pOutBuf, int size, ClntInfo &clntInfo)
{
    if ( size < 0 || 
        size > FTPBUF_DEFSIZE )
        return -1;
    
    int strLen=0;
    char pBuf[1024];
    uint32_t tm;
    char * ptr = pOutBuf;

    int len = Packer::unpack(ptr, tm);
    size -= len;
    ptr += len;
    clntInfo.setStartTm(tm);
    
    len = Packer::unpack(ptr, tm);
    size -= len;
    ptr += len;
    clntInfo.setInstallTm(tm);
    
    len = Packer::unpackLenStr(ptr, pBuf, strLen); //clntVer
    size -= len;
    ptr += len;
    pBuf[strLen]='\0';
    clntInfo.setClntVer(pBuf);
    
    len = Packer::unpackLenStr(ptr, pBuf, strLen); //hostName
    size -= len;
    ptr += len;
    pBuf[strLen]='\0';
    clntInfo.setHostName(pBuf);
    
    len = Packer::unpackLenStr(ptr, pBuf, strLen); //m_osName
    size -= len;
    ptr += len;
    pBuf[strLen]='\0';
    clntInfo.setOsName(pBuf);

    len = Packer::unpackLenStr(ptr, pBuf, strLen); //osVer
    size -= len;
    ptr += len;
    pBuf[strLen]='\0';
    clntInfo.setOsVer(pBuf);
    
    len = Packer::unpackLenStr(ptr, pBuf, strLen); //arch
    size -= len;
    ptr += len;
    pBuf[strLen]='\0';
    clntInfo.setArch(pBuf);
    return 0;
}

int ThinProto::encode_CLNT_DEL_FILES(ByteBuf_t &byteBuf, uint16_t reserved, const StrVector_t &fileList)
{
     return Proto::encode_StrVector_Body(byteBuf, RT_THIN, CLNT_DEL_FILES, reserved, fileList);   
}

int ThinProto::encode_CLNT_DEL_DIR(char *pOutBuf, int len, uint16_t reserved, const std::string &dirName)
{
    return Proto::encode_Str_Body(pOutBuf, len, RT_THIN, CLNT_DEL_DIR, reserved, dirName);  
}

int ThinProto::encode_CLNT_ADD_FILE(char *pOutBuf, int len, uint8_t mType, uint16_t reserved, const std::string &fileName)
{
    return Proto::encode_Str_Body(pOutBuf, len, mType, CLNT_ADD_FILE, reserved, fileName);  
}

int ThinProto::encode_SVRASKCLNT_REGET_FULLDIR(char* pOutBuf, int size, uint16_t reserved, const std::string & dirId, int grpMemCnt)
{
    int tot = PacketHeader::size();
    char *ptr = pOutBuf + PacketHeader::size();
    
    int len = Packer::packLenStr(ptr, 1024, dirId.c_str(), dirId.size()); // server dirID(string)
    tot += len;
    ptr += len;
    
    len = Packer::pack(ptr, 1024, grpMemCnt); // how many member in group
    tot += len;
    ptr += len;
    
    return len;
}

int ThinProto::decode_SVRASKCLNT_REGET_FULLDIR(char* pOutBuf, int size, uint16_t& reserved, std::string& dirId, int& clntCnt)
{
    if ( size < 0 || 
        size > FTPBUF_DEFSIZE )
        return -1;
    
    int strLen=0;
    char pBuf[1024];
    char * ptr = pOutBuf;

    int len = Packer::unpackLenStr(ptr, pBuf, strLen); //dirId
    size -= len;
    ptr += len;
    pBuf[strLen]='\0';
    dirId = pBuf;
    
    len = Packer::unpack(ptr, clntCnt);
    size -= len;
    return !size ? 0 : -1;
}



int ThinProto::encode_CLNT_RENAME_FS(ByteBuf_t &byteBuf, uint16_t reserved, 
    const std::string & oldName, const std::string &newName)
{
    StrVector_t v;
    v.push_back(oldName);
    v.push_back(newName);
    return Proto::encode_StrVector_Body(byteBuf, RT_THIN, CLNT_RENAME_FS, reserved, v);     
}


int ThinProto::decode_CLNT_DEL_FILES(char *pOutBuf, int len, StrVector_t &fileLst)
{
    return Proto::decode_StrVector_Body(pOutBuf, len, fileLst);  
}

int ThinProto::decode_CLNT_RENAME_FS(char *pOutBuf, int len, std::string & oldName, std::string &newName)
{
    if ( len < 0 || 
        len > FTPBUF_DEFSIZE )
        return -1;
    
    std::vector<std::string> v;
    int ret  = Proto::decode_StrVector_Body(pOutBuf, len, v);    
    assert(v.size() == 2);
    oldName     = v[0];
    newName     = v[1];
    return ret;
}

int ThinProto::encode_SVRCMD_NEWDIR(ByteBuf_t & byteBuf, uint16_t reserved, const std::vector<std::string> &v)
{
    return Proto::encode_StrVector_Body(byteBuf, RT_SVR_CMD,  ThinCmd::NEW_CONF, reserved, v);
}

int ThinProto::decode_SVRCMD_NEWDIR(char *pOutBuf, int size, std::vector<std::string> &v)
{
    return Proto::decode_StrVector_Body(pOutBuf, size, v);
}

int ThinProto::encode_SVRCMD_SHUTDOWN(char *pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_SVR_CMD, ThinCmd::SHUTDOWN_AGENT, reserved);
}

int ThinProto::encode_SVRCMD_DELETE(char *pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_SVR_CMD, ThinCmd::DELETE_AGENT, reserved);
}

int ThinProto::encode_SVRCMD_RESTART(char *pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_SVR_CMD, ThinCmd::RESTART_AGENT, reserved);
}

int ThinProto::encode_SVRCMD_FORCE_GETALL(char* pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_SVR_CMD, ThinCmd::FORCE_GETALL, reserved);
}



