#include "conf.h"
#include "jsonparser.h"
#include <common/fsfunc.h>
#include <util/logger.h>
#include <iostream>


Conf::Conf()
{    
}
Conf::~Conf()
{
}
    
int Conf::loadFromFile(const char* fileName)
{
    try
    {
        std::string fileContent;
        m_confFile = fileName;
        assert( fsfunc::readFileAll(fileName, fileContent) == 0);
        return loadFromStr(fileContent);
    }
    catch (const std::exception& e  )
    {
        std::cerr << e.what();
        return -1;
    }
}

int Conf::loadFromStr(const std::string& content)
{
    JsonParser  parser;
    assert(parser.trJson2Map(content, m_confMap) == 0);
    return 0;
}


int Conf::write()
{
    return write(m_confMap);
}

int Conf::write(const ConfMap_t &confMap)
{
    JsonParser  parser;
    std::string outStr;

    parser.trMap2Json(confMap, outStr);
    return fsfunc::writeFileAll(m_confFile, outStr);
}

const std::string & Conf::getLogFile()
{
    return m_confMap["log"]["filename"];
}

const std::string & Conf::getLogLevel()
{
    return m_confMap["log"]["level"];
}

const std::string & Conf::getTcpSvrIp()
{
    return m_confMap["server"]["tcp:ip"];
}

uint16_t Conf::getTcpSvrPort()
{
    return atoi(m_confMap["server"]["tcp:port"].c_str());
}

const std::string & Conf::getSslSvrIp()
{
    return m_confMap["server"]["ssl:ip"];
}

const std::string& Conf::getSslSvrPortS()
{
    return m_confMap["server"]["ssl:port"];
}

uint16_t Conf::getSslSvrPort()
{
    return atoi(getSslSvrPortS().c_str());
}

const std::string & Conf::getDocRoot()
{
    return m_confMap["ftp"]["docroot"];
}

bool Conf::getIsZip()
{
    return false;
    //return m_confMap["ftp"]["zip"] == "1";
}


const std::string & Conf::getSslKey()
{
    return m_confMap["ssl"]["key"];
}

const std::string & Conf::getSslCert()
{
    return m_confMap["ssl"]["cert"];
}
