#include "fsfunc.h"

#include <iostream>
#include <iterator>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <openssl/md5.h>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <unordered_set>
#include <set>
#include <util/logger.h>

#define DELFILE "delFiles.txt"
#define expirationTm  120

namespace fs = boost::filesystem;
namespace fsfunc
{

int getDirVolume(const std::string & dirName, uint32_t *pCnt, uint32_t *pTotSz, StrVector_t * pFlist)
{
    if( !fs::is_directory(dirName))
        return -1;
    uint32_t cnt = 0;
    uint64_t tot =0;
    fs::recursive_directory_iterator rdi(dirName);  
    fs::recursive_directory_iterator end_rdi;
    for (; rdi != end_rdi; rdi++)
    {
        const std::string & eachFile = (*rdi).path().string();
        if (fs::is_regular_file(eachFile))
        {
            if (pFlist != NULL)
            {
                pFlist->push_back(eachFile);
            }
            if (pTotSz != NULL)
                tot += fs::file_size(eachFile);
            cnt++;
        }
    }
    *pCnt = cnt;
    if (pTotSz != NULL)
        *pTotSz = int(tot / 1024);
    return 0;        
}

int getDirFtpInfo(const std::string & docroot, const std::string & dirName, uint32_t cutoffTm,
                    uint32_t *pTotSz, StrVector_t * pFlist)
{
    uint64_t tot =0;
    fs::recursive_directory_iterator rdi(dirName);  
    fs::recursive_directory_iterator end_rdi;
    for (; rdi != end_rdi; rdi++)
    {
        const std::string & eachFile = (*rdi).path().string();
        fs::path f(eachFile ) ;
        if ( fs::is_regular_file( f ) && fs::last_write_time( f ) >= cutoffTm )
        {
            pFlist->push_back(eachFile.substr(docroot.size()));
            tot += fs::file_size(eachFile);
        }
    }
    if (pTotSz != NULL)
        *pTotSz = int(tot / 1024);
    return 0;
}

int getLongestPaths(const std::string &docroot, const std::string &dirName, StrVector_t & retLst)
{
    fs::path path(dirName);
    if( ! fs::is_directory(path))
        return -1;

    StrVector_t  lst;
    fs::directory_iterator end_itr;
    for (fs::directory_iterator itr(path); itr != end_itr; ++itr)
    {
        if (fs::is_directory(itr->path()))
        {
            lst.push_back(itr->path().string());
        }

    }
    if ( lst.size() == 0 )
    {
        retLst.push_back(dirName.substr(docroot.size()));
    }
    else
    {
        for (const std::string & f : lst)
        {
           getLongestPaths(docroot, f, retLst); 
        }
    }
    return 0;
}

bool dirHasChild(const char *pDir)
{
    if(!boost::filesystem::is_directory(pDir))
        return false;

    boost::filesystem::directory_iterator end_it;
    boost::filesystem::directory_iterator it(pDir);
    return (it != end_it);
}

//remove all dirs without any file
//dir: starting dir
void delEmptyDirs(const std::string & dir)
{
     StrVector_t retLst;
     getLongestPaths("", dir, retLst);
     char pBuf[1024];
     char *ptr;
     for(const std::string & d : retLst)
     {
         memcpy(pBuf, d.c_str(), d.length());
         pBuf[d.length()]='\0';
         while ( !dirHasChild(pBuf) )
         {
             fs::remove(pBuf);
             ptr = strrchr(pBuf, fs::path::preferred_separator); 
             if(ptr == NULL)
                 break;
             *ptr = '\0';
         }
     }
}

int splitStr(const std::string &s, char delim, std::vector<std::string> &retV)
{
    std::stringstream ss(s);
    std::string item;
    while (getline(ss, item, delim))
    {
        retV.push_back(item);
    }
    return 0;
}
//{"/tmp/dir1/dir2", "/tmp/dir1/dir3"}, "/tmp/".size(), =>{"/tmp/dir1", "/tmp/dir1/dir2", "/tmp/dir1/dir3"} without "/" and "/tmp/"
int expandLongestPaths(const StrVector_t & longestPath, int preffixLen, StrVector_t & allDirs)
{
    std::set<std::string> oset;
    for (const std::string &longest: longestPath)
    {
        StrVector_t fl;
        std::string vpath;
        splitStr(longest, fs::path::preferred_separator, fl);
        for(std::string item : fl)
        {
			item += fs::path::preferred_separator;
            vpath += item;
            if ((int)vpath.size() > preffixLen)
                oset.insert(vpath);
        }
    }
    allDirs.insert(allDirs.end(), oset.begin(), oset.end());    

    return 0;
}

int createMissingDirs(const std::string & docroot, const StrVector_t &flst)
{
    for( const std::string & e : flst)
    {
        if ( (docroot + e).size() > 1)
            fs::create_directories(docroot + e);
    }
    return 0;
}

bool isBitOn(uint16_t reserved, uint16_t bitmask)
{
    return reserved & bitmask; 
}

void setBitOn(uint16_t &reserved, uint16_t bitmask)
{
    reserved |= bitmask;
}

void setBitOff(uint16_t &reserved, uint16_t bitmask)
{
    reserved &= ~bitmask;    
}

std::string md5sum(const std::string &fileName)
{
    char file_buffer[4096];
    std::ostringstream oss;
    unsigned char digest[MD5_DIGEST_LENGTH] = {};

    MD5_CTX ctx;
    MD5_Init(&ctx);

    std::ifstream ifs(fileName, std::ios::binary);
    while (ifs.read(file_buffer, sizeof(file_buffer)) || ifs.gcount())
    {
        MD5_Update(&ctx, file_buffer, ifs.gcount());
    }
    MD5_Final(digest, &ctx);
    for(unsigned i=0; i < MD5_DIGEST_LENGTH; i++)
    {
        oss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(digest[i]);
    }
    return oss.str();
}


int readFileAll(const std::string& fileName, std::string& outStr)
{
    char pBuf[10240];
    std::fstream fstrm; 
    fstrm.open(fileName , std::ios::in | std::ios_base::binary);
    if(fstrm.fail())
    {
        return -1;
    }
    outStr.reserve(10240);
    fstrm.seekg (0, fstrm.beg);
    while ( !fstrm.eof() )
    {
        fstrm.read(pBuf, sizeof(pBuf));
        outStr.append(pBuf, fstrm.gcount());
    }
    fstrm.close();
    return 0;
}

int writeFileAll(const std::string& fileName, const char *pBuf, int len)
{
    std::fstream fstrm; 
    fstrm.open (fileName, std::ofstream::out | std::ios_base::binary );
    if (fstrm.fail())
    {
        return -1;
    }
    fstrm.write(pBuf, len);  
    fstrm.close();
    return 0;

}

int writeFileAll(const std::string& fileName, const std::string &inStr)
{
    return writeFileAll(fileName, inStr.c_str(), (int)inStr.size());
}

static void appendOldDelFile(void * args1, void * args2)
{
    std::string sub1;
    uint32_t tm;
    std::string *pLine                 = (std::string *)args1;
    StrVector_t  *pFileList      = (StrVector_t *)args2;
    char *ptr = ::strchr((char *)(pLine->c_str()), '|');
    if (ptr != NULL)
    {
        sub1.insert(sub1.end(), (char *)(pLine->c_str()), ptr);
        tm = atoi(sub1.c_str());
        if ( (tm + expirationTm) > (uint32_t) time(NULL)) //keep up to 2 mins
        {
            pFileList->push_back(*pLine );
        }        
    }
}

//read string from pos to a deliminator
int substrByDeliminator(const std::string& inStr, int pos, uint8_t deliminator, std::string & outStr)
{
    std::size_t nPos = inStr.find(deliminator, pos);
    if (nPos == std::string::npos)
        return -1;
    outStr = inStr.substr(pos, nPos);
    return 0;
}

//read string from pos to a deliminator
//e.g. "100/tmp1/tmp2" => str1="100, str2="tmp1/tmp2"
int substrByDeliminator(const std::string& inStr, int pos, uint8_t deliminator, std::string & str1, std::string & str2)
{
    std::size_t nPos = inStr.find(deliminator, pos);
    if (nPos == std::string::npos)
        return -1;
    
    str1 = inStr.substr(pos, nPos);
    str2 = inStr.substr(nPos + 1);
    return 0;
    
}


int logDelFiles(const std::string&docRoot, const StrVector_t & newDels)
{
    StrVector_t oldFiles;
    if( !newDels.size() )
        return 0;
    std::string dirId;
    if (substrByDeliminator(newDels[0], 0, fs::path::preferred_separator, dirId) < 0)
        return -1;
    
    char delFile[256];
    snprintf(delFile, sizeof(delFile), "%s%s/%s", docRoot.c_str(), dirId.c_str(), DELFILE );
    std::fstream fstrm;
    // 2) read valid old lists
    if ( fs::is_regular_file(delFile))
        foreach_txtline(delFile, appendOldDelFile, &oldFiles);
    // 3) merge old and new 
    fstrm.open(delFile , std::ios::out);
    if( !fstrm.fail())
    {
        for (auto &f : oldFiles)
            fstrm << f << std::endl;
        for (auto &f : newDels)
            fstrm << (uint32_t) time(NULL) << '|' << f << std::endl;
    }
    fstrm.close();
    return 0;
}

//for each line in txt file
void foreach_txtline(const std::string& fileName, cb_fn2 cbFn, void *args)
{
    std::string line;
    std::fstream fstrm;
    fstrm.open(fileName , std::ios::in);
    if( fstrm.fail())
        return ;
    
    fstrm.seekg (0, fstrm.beg);
    while ( !fstrm.eof() )
    {
        std::getline(fstrm, line);
        cbFn(&line, args);
    }
    fstrm.close();
}

//e.g c:/tmp/file1.txt => 25/file1.txt    
//c:/tmp/subdir/file1.txt => 25/subdir/file1.txt
int mapDir2Id(const Dir2IdArr_t &dir2IdArr, 
              const std::string &localFs, std::string &mapFs)
{
    std::string tmpStr;
    for (const std::pair<std::string, std::string> &dirId : dir2IdArr)
    {
        if(localFs.find(dirId.first, 0) != std::string::npos)
        {
            tmpStr= localFs.substr(dirId.first.size() );
            mapSlash2Svr( tmpStr );
            mapFs = dirId.second + tmpStr;
            return 0;
        }
    }
    return -1;
}

//e.g 25/file1.txt => c:/tmp/file1.txt
// (25/subdir)/file1.txt => (c:/tmp/subdir)/file1.txt
int mapId2Dir(const Dir2IdArr_t &dir2IdArr, 
              const std::string &mapFs, std::string &localFs)
{
    std::string tmpStr;
    for (const std::pair<std::string, std::string> &dirId : dir2IdArr) 
    {
        if(mapFs.find(dirId.second, 0) != std::string::npos)
        {
            tmpStr= mapFs.substr(dirId.second.size() );
            mapSlash2Svr( tmpStr );
            localFs = dirId.first + tmpStr;
            return 0;
        }
    }
    return -1;
    
}
/*
int mapId2Dir(const Dir2IdArr_t &dir2IdArr, 
              const std::string &mapFs, std::string &localFs)
{
    char pBuf[8];
    char ch = fs::path::preferred_separator;
    if (mapFs.size() == 1)
    {
        localFs = mapFs;
        return -1;
    }
    char *ptr = strchr((char*)mapFs.c_str(), ch);
    if ( !ptr)
    {
        localFs = mapFs;
        return -1;
    }
    int len = (int)(strchr(mapFs.c_str(), ch) - mapFs.c_str());
    memcpy(pBuf, mapFs.c_str(), len);
    pBuf[len] = '\0';
    std::string tmpStr;
    for (const std::pair<std::string, std::string>& dirId : dir2IdArr)
    {
        if(dirId.second == pBuf)
        {
            tmpStr  = mapFs.substr( len + 1 );
            mapSlash2Local(tmpStr);
            localFs = dirId.first + tmpStr; //only one back slash is needed
            return 0;
        }
    }
    localFs = mapFs;
    return -1;
}
 */

//e.g vector of c:/tmp/file1.txt => vector of 25/file1.txt  
int mapDir2Id(const Dir2IdArr_t &dir2IdArr, 
            const StrVector_t &localFs, StrVector_t &mapFs)
{
    std::string tmp;
    for(const std::string & f : localFs)
    {
        mapDir2Id(dir2IdArr, f, tmp);
        mapFs.push_back(tmp);
    }
    return 0;
}

//e.g vector of 25/file1.txt => vector of c:/tmp/file1.txt
int mapId2Dir(const Dir2IdArr_t &dir2IdArr, 
            const StrVector_t &mapFs, StrVector_t &localFs)
{
    std::string tmp;
    for(const std::string & f : mapFs)
    {
        mapId2Dir(dir2IdArr, f, tmp);
        if ( !tmp.empty())
            localFs.push_back(tmp);
    }
    return 0;
}

int remove(const std::string &docroot, const StrVector_t &fileLst)
{
    for (const std::string &f : fileLst)
    {
        std::string absFile = docroot + f;
        if (fs::is_directory(absFile))
            fs::remove_all(absFile.c_str());
        else
            fs::remove(absFile.c_str());
    }
    return 0;
}

int getMapFilesOfDir(const std::string &docroot, const std::string &dirName, StrVector_t &fileLst)
{
    //list all files under dir
    if( !fs::is_directory(dirName))
        return -1;

    fs::recursive_directory_iterator rdi(dirName);  
    fs::recursive_directory_iterator end_rdi;
    for (; rdi != end_rdi; rdi++)
    {
        const std::string & eachFile = (*rdi).path().string();
        if (fs::is_regular_file(eachFile))
        {
            fileLst.push_back(eachFile.substr(docroot.size()) );
        }
    }
    return 0;    
}

int getAllDirs(const std::string docroot, const std::string &dirName, StrVector_t &fileLst)
{
    //list all files under dir
    if( !fs::is_directory(dirName))
        return -1;
	std::string tmpStr;
    fs::recursive_directory_iterator rdi(dirName);  
    fs::recursive_directory_iterator end_rdi;
    for (; rdi != end_rdi; rdi++)
    {
        const std::string & each = (*rdi).path().string();
        if (fs::is_directory(each))
        {
			tmpStr = each.substr(docroot.size());
			tmpStr += fs::path::preferred_separator;
            fileLst.push_back( tmpStr);
        }
    }
    return 0;    
}

StrVector_t set1Diff(const StrVector_t &lhs, const StrVector_t &rhs)
{
    std::set<std::string> s1( lhs.begin(), lhs.end() );
    std::set<std::string> s2( rhs.begin(), rhs.end() );
    StrVector_t result;

    std::set_difference( s1.begin(), s1.end(), s2.begin(), s2.end(),
        std::back_inserter( result ) );
    return result;        
}

void replaceOne(std::string& subject, const std::string& oldStr,
                          const std::string& newStr)
{
    size_t pos = 0;
    if((pos = subject.find(oldStr, pos)) != std::string::npos)
    {
         subject.replace(pos, oldStr.length(), newStr);
    }
}

void mapSlash2Local(std::string &fsName)
{
    if (fs::path::preferred_separator == '\\') //windows client, no need for linux
        boost::replace_all(fsName, "/", "\\");
}

void mapSlash2Svr(std::string &fsName)
{
    if (fs::path::preferred_separator == '\\') //windows client, no need for linux
        boost::replace_all(fsName, "\\", "/");
}

void FStream::open (const char* filename,
        ios_base::openmode mode)
{
    m_fileName = filename;
    basic_fstream<char>::open(filename, mode);
}

bool FStream::isTmpFile(const std::string & prefix, std::string &orig) const
{
    int len = (int)m_fileName.size();
    if (m_fileName.substr(len - prefix.size()) == prefix)
    {
        orig = m_fileName.substr(0, len - prefix.size());
        return true;
    }
    return false;
}

}