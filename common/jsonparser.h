#ifndef __JSONPARSER_H
#define __JSONPARSER_H

#include <map>
#include <string>

typedef std::map<std::string, std::map<std::string, std::string>> JsonMap_t;

class JsonParser 
{
public:
    int trJson2Map(const std::string &inStr, JsonMap_t &jsonMap);
    int trMap2Json(const JsonMap_t &jsonMap, std::string &outStr);
};

#endif