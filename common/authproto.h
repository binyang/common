#ifndef __AUTHPROTO__H
#define __AUTHPROTO__H

#include <inttypes.h>
#include <string>
enum
{
    AUTH_SVR_REJECT        = 1,
    AUTH_CLNT_REQ,
    AUTH_SVR_ACCEPT
};

typedef struct
{
    uint8_t     linkType;       //UP/DOWN link
    uint8_t     mode;           //MASTER/SALVE
    uint16_t    userId;         //userId % 2^16
    std::string uuid;
    std::string user;
    std::string passwd;
    std::string dirs;
}Crenditenal_t;

class AuthProto
{
public:
    
    static int encode_AUTH_SVR_ACCEPT(char *pOutBuf, int len, uint16_t reserved); //header only without body
    static int encode_AUTH_SVR_REJECT(char *pOutBuf, int len, uint16_t reserved); //header only without body

    static int encode_AUTH_CLNT_REQ(char *pOutBuf, int size, uint16_t reserved, const Crenditenal_t & crend);
    static int decode_AUTH_CLNT_REQ(char *pOutBuf, int len, Crenditenal_t &crend);    
   
};


#endif 