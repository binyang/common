#ifndef __CONF_H
#define __CONF_H

#include <map>
#include <string>

class Conf 
{
    
public:
    typedef std::map<std::string, std::map<std::string, std::string>> ConfMap_t;

    Conf();
    virtual ~Conf();
    int write(const ConfMap_t &confMap);
    int write();
   
    virtual int loadFromFile(const char *fileName);
    virtual int loadFromStr(const std::string & content);
    virtual bool isValid() = 0;
    
    const std::string & getLogFile();
    const std::string & getLogLevel() ;
    
    const std::string & getTcpSvrIp() ;
    uint16_t            getTcpSvrPort() ;
    
    const std::string & getSslSvrIp() ;
    uint16_t            getSslSvrPort() ;
    const std::string & getSslSvrPortS() ;
    
    const std::string & getDocRoot() ;
    bool                getIsZip() ;
    
    ConfMap_t         & getConfMap()
    {   return m_confMap;       }
    
    const std::string & getSslKey();
    const std::string & getSslCert();
    
protected:
    int readJson2Map();
    
protected:
    ConfMap_t   m_confMap;
    std::string m_confFile;
};

#endif