#include "packetheader.h"
#include <memory.h>

PacketHeader::PacketHeader()
{
    init();
}

void PacketHeader::init()
{
    memset(this, 0x0, sizeof(PacketHeader));
}

bool PacketHeader::isValid()
{
    return m_crc == crc16_ccitt (( char* ) &m_mType , size() - 2);
}

//return:
//      > 0 : consumed bytes
//      =0  : error
//      < 0 : needed bytes

bool PacketHeader::unpack ( const char * pBuf, int bufLen )
{
    if ( bufLen < size() )
        return false;

    memmove ( this, pBuf, size() );
    return isValid();
}


void PacketHeader::buildHeader(uint8_t mType, uint8_t sType, uint16_t reserved, uint32_t totLen)
{
    init();
    m_mType = mType;
    m_sType = sType;
    m_reserved = reserved;
    m_totLen = totLen;

    calCRC16();
}

uint16_t PacketHeader::calCRC16()
{
    m_crc = crc16_ccitt (  ( char* ) &m_mType , ( sizeof( PacketHeader) - 2 ) );
    return m_crc;
}
