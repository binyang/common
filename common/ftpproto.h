#ifndef __FTPPROTO__H
#define __FTPPROTO__H

#include "stdinl.h"
#include <common/packetheader.h>
#include <vector>
#include <string>

/******************* message flow
basically any file/dir client sends must be mapped to dirId (mapDir2Id)
any file/dir client receives must be mapped to local(mapId2Dir)

put file:
    1.CLNT_PUT_REQ(clnt send with FtpCtx, path mapping) 
    2.SVR_ACK_PUT(svr reply with error code uint32_t)
    3)REQ_PUT_BLNKDIRS(len = 0, nothing is sent)
    4)CLNT_PUT_FILE(clnt send file name std::string + md5sum, path mapping )
    5)SVR_CMP_PUTFILE(server reply changed or not)
        client compare its own md5, if same, go to 7)
    6)FTP_FILE_DATA(clnt send)
    7)FTP_EOF(clnt send with tm(0) + md5sum)
    8) done
    
put dir:
    1.CLNT_PUT_REQ(clnt send with FtpCtx, path mapping) 
    2.SVR_ACK_PUT(svr reply with error code uint32_t)
    3)REQ_PUT_BLNKDIRS(client send empty longest dir, path mapping)  
      loop 3) make sure packet is not too big
    4)CLNT_PUT_FILE(clnt send file name std::string + md5sum, path mapping)
    5)SVR_CMP_PUTFILE(server reply changed or not, update fscache hit=1)
        client compare its own md5, if same, go to 8)
    6)FTP_FILE_DATA(clnt send)
    7)FTP_EOF(clnt send with tm(0)+ md5sum)
    8) clnt go back 3 until done
    9) CLNT_PUTDIR_DONE (client notify svr, svr delete unused files)
    
get file:
    1.CLNT_GET_REQ(clnt send with FtpCtx, path mapping) 
    2.SVR_ACK_GET(svr reply ".")
    3)SVR_RESP_FLST(svr reply the file name, client path mapping)
        if ret bodylen == 0, task should be done
        loop 3) make sure packet is not too big    
    4)SVR_RESP_FLST_DONE (svr reply, client can get file now)     
    5)CLNT_GET_FILE(clnt send file name std::string, path mapping + md5sum)
    6)FTP_FILE_DATA(svr send)
    7)FTP_EOF(svr send with syncTm + md5sum)

get dir:
    1.CLNT_GET_REQ(clnt send with FtpCtx, path mapping) 
    2.SVR_ACK_GET(svr reply with the longest dir)
        delete unused dirs;
    3)SVR_RESP_FLST(svr reply StrVector_t, client path mapping)
        if ret bodylen == 0, task should be done
        loop 3) make sure packet is not too big
    4)SVR_RESP_FLST_DONE (svr reply, client can get file now)     
    5)CLNT_GET_FILE(clnt send with std::string, path mapping + md5sum)
    6)FTP_FILE_DATA(svr send)
    7)FTP_EOF(svr send with syncTm + md5sum)
    8) clnt go back 5 until done

    
********************************/

enum
{
    CLNT_PUT_REQ        = 1,     //client send request (FtpCtx)
    SVR_ACK_PUT,                //svr reply ok or error code 

    REQ_PUT_BLNKDIRS,            //client send the longest dir names
    SVR_CMP_PUTFILE,             //server reply md5sum       
    CLNT_PUT_FILE,               //client send the file's name (std::string)
    FTP_FILE_DATA,
    FTP_EOF,                    //include md5sum
    CLNT_PUTDIR_DONE,           //only header
    CLNT_GET_REQ,               //client get file request (FtpCtx)
    SVR_ACK_GET,                //server ack  (the longest dir names)
    SVR_RESP_FLST,              //server send the StrVector_t (file list break into pieces as it might be too big)
    SVR_RESP_FLST_DONE,         //server send          
    CLNT_GET_FILE                //client ask the file's name (std::string)
};
//FtpCtx type
enum
{
    CTX_PUT_FILE        = 1,
    CTX_PUT_ALLDIR,
    CTX_PUT_INCDIR,
    CTX_GET_FILE,
    CTX_GET_ALLDIR,
    CTX_GET_INCDIR
};

class FtpCtx
{
public:
    FtpCtx(uint8_t ctxType_, uint32_t syncTm_, uint32_t totalSize_, uint32_t fileCnt_,  std::string fsName_); 
    FtpCtx(const FtpCtx & rhs);
    bool isDirCtx();    
    
    uint8_t     ctxType;
    uint32_t    syncTm;    //if it is dir, FtpGetClient use it as filelist cutoff time. FtpPutClient does not use so far
    uint32_t    totalSize;
    uint32_t    fileCnt;
    std::string fsName;
};

class FtpProto
{
public:
    static int encode_CLNT_PUT_REQ(char *pOutBuf, int len, uint16_t reserved, const FtpCtx &ftpCtx);
    static int encode_SVR_ACK_PUT(char *pOutBuf, int len, uint16_t reserved, int errorCode);
    
    static int encode_REQ_PUT_BLNKDIRS(ByteBuf_t &byteBuf, uint16_t reserved, StrVector_t &bigList);
    static int encode_CLNT_PUT_FILE(ByteBuf_t &byteBuf, uint16_t reserved, 
                                    const std::string &fileName, const std::string &clntMd5);
    static int decode_CLNT_PUT_FILE(char *pOutBuf, int len, std::string &fileName, std::string &clntMd5);    
    
    static int encode_SVR_CMP_PUTFILE(char *pOutBuf, int len, uint16_t reserved, bool bChanged);
    
    static int encode_FTP_FILE_DATA(char *pOutBuf, int len, int bodyLen, uint16_t reserved);
    static int encode_FTP_EOF(char *pOutBuf, int len, uint16_t reserved, uint32_t tm, const std::string &md5 );
    static int decode_FTP_EOF(char *pOutBuf, int len, uint32_t &tm,  std::string &md5 );
    
    
    static int encode_CLNT_GET_REQ(char *pOutBuf, int len, uint16_t reserved, const FtpCtx &ftpCtx);   
    static int encode_CLNT_GET_FILE(ByteBuf_t &byteBuf, uint16_t reserved, 
                                    const std::string &fileName, const std::string &clntMd5);
    
    static int decode_CLNT_GET_FILE(char *pOutBuf, int len, std::string &fileName, std::string &clntMd5);  
    
    static int encode_SVR_RESP_FLST(ByteBuf_t &byteBuf, uint16_t reserved, StrVector_t &bigList);
    static int encode_SVR_RESP_FLST_DONE(char *pByteBuf, int len, uint16_t reserved);
    
    static int encode_SVR_ACK_GET(ByteBuf_t &byteBuf, uint16_t reserved, const StrVector_t &fileList);
    
    static int decode_FILE_CTX(char *pOutBuf, int len, FtpCtx &ftpCtx);

    static int encode_CLNT_PUTDIR_DONE(char *pOutBuf, int len, uint16_t reserved);
    
private:
    static int encode_FILE_CTX(char *pOutBuf, int len, uint8_t sType, uint16_t reserved, const FtpCtx &ftpCtx);
};


#endif 