#ifndef __CLNTINFO__H
#define __CLNTINFO__H

#include <util/tsingleton.h>
#include <string>
#include <time.h>
#include <stdint.h>
enum
{
    WORKMODE_PUT = 1,
    WORKMODE_GET,
    WORKMODE_BOTH
};

class ClntInfo :  public TSingleton<ClntInfo>
{
    friend class TSingleton<ClntInfo>;
public:
    void setNativeVars(std::string & hostName, std::string &osName, std::string &osVer,  std::string &arch);
    uint32_t getInstallTm() const
    {   return m_installTm;     }
    
    uint32_t getStartTm() const
    {   return m_startTm;     }

    const std::string &getClntVer() const
    {   return m_clntVer;      }

    const std::string &getHostName() const
    {   return m_hostName;      }
    
    const std::string &getOsName() const
    {   return m_osName;        }
    
    const std::string &getOsVer() const
    {   return m_osVer;         }

    const std::string &getArch() const
    {   return m_arch;          }

    void setInstallTm(uint32_t tm )
    {   m_installTm = tm;     }
    
    void setStartTm(uint32_t tm)
    {   m_startTm = tm;     }

    void setClntVer(const char *clntVer)
    {   m_clntVer = clntVer;      }

    void setHostName(const char *hostName)
    {   m_hostName = hostName;      }
    
    void setOsName(const char *osName)
    {   m_osName = osName;        }
    
    void setOsVer(const char *osVer)
    {   m_osVer = osVer;         }

    void setArch(const char *arch)
    {   m_arch = arch;          }    
    
    void debug();
private:
    ClntInfo();
    virtual ~ClntInfo(); 

private:
    uint32_t    m_startTm;
    uint32_t    m_installTm;
    
    std::string m_clntVer;
    std::string m_hostName; 
    std::string m_osName;
    std::string m_osVer;
    std::string m_arch;
    
};


#endif 