
#ifndef GZIPSTREAM_H
#define GZIPSTREAM_H
#include <common/stdinl.h>
#include <zlib.h>

enum
{
    ZIP_DEF     = 1,
    ZIP_INF
};

class GzipStream
{
public:

    GzipStream(int mode, ByteBuf_t* pLBuf, int level = Z_DEFAULT_COMPRESSION);
    ~GzipStream();
    int compress (const char *pInBuf, int dataLen, int offset );
    int uncompress (const char *pInBuf, int dataLen);
    int initDef();
    int initInf();

    ByteBuf_t * getByteBuf()
    {   return m_pByteBuf;      }
    
    int getZipErr(int errno, std::string &errMsg);

private:
    int release();
private:  
    z_stream    m_zstrm;
    int         m_errno;
    int         m_zLevel;
    int         m_zipMode;
    ByteBuf_t   *m_pByteBuf;
    
};



#endif
