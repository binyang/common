#ifndef __THINPROTO__H
#define __THINPROTO__H

#include "clntinfo.h"
#include <common/stdinl.h>
#include <common/packetheader.h>
#include "proto.h"
#include <string>
enum ThinCmd
{
    THIN_CLNT_INFO = 1,

    THIN_LIVE_PING,     
    THIN_LIVE_PONG,
    THIN_HANDSHAKE,
    CLNT_DEL_FILES, 
    CLNT_DEL_DIR,
    CLNT_RENAME_FS,
    
    CLNT_ADD_FILE,      //infom client to get new file 
    CLNT_ADD_DIR,      // to get full dir again
    NEW_CONF,

    DELETE_AGENT,
    RESTART_AGENT,            //client restart
    SHUTDOWN_AGENT,
    FORCE_GETALL,
 
    AUTH_FAIL,    
    CONN_ERR,
    HEAD_ERR
};


class ThinProto : public Proto
{
    
public:
    //header + uint64_t time + pending string
    static int encode_THIN_LIVE(char *pOutBuf, int size, uint8_t liveType, uint16_t reserved, uint64_t tm, const std::string & cmds);

        
    static int encode_THIN_CLNT_INFO(char *pOutBuf, int size, const ClntInfo &clntInfo);

    static int decode_THIN_CLNT_INFO(char *pOutBuf, int size, ClntInfo &clntInfo);

    static int encode_CLNT_ADD_FILE(char *pOutBuf, int len, uint8_t mType, uint16_t reserved, const std::string &fileName);
    static int encode_CLNT_DEL_FILES(ByteBuf_t &byteBuf, uint16_t reserved, const StrVector_t &fileList);
    static int encode_CLNT_DEL_DIR(char *pOutBuf, int len, uint16_t reserved, const std::string &dirName);
    static int encode_CLNT_RENAME_FS(ByteBuf_t &byteBuf, uint16_t reserved, 
        const std::string & oldName, const std::string &newName);
    
    static int decode_CLNT_DEL_FILES(char *pOutBuf, int len, StrVector_t &fileLst);
    static int decode_CLNT_RENAME_FS(char *pOutBuf, int len, std::string & oldName, std::string &newName);
  
    static int encode_SVRCMD_NEWDIR(ByteBuf_t &byteBuf, uint16_t reserved, const std::vector<std::string> &v);
    static int decode_SVRCMD_NEWDIR(char *pOutBuf, int len, std::vector<std::string> &v);
    
    static int encode_SVRCMD_SHUTDOWN(char *pOutBuf, int len, uint16_t reserved);
    static int encode_SVRCMD_DELETE(char *pOutBuf, int len, uint16_t reserved);
    static int encode_SVRCMD_RESTART(char *pOutBuf, int len, uint16_t reserved);
    static int encode_SVRCMD_FORCE_GETALL(char *pOutBuf, int len, uint16_t reserved);
    
    static int encode_SVRASKCLNT_REGET_FULLDIR(char *pOutBuf, int len, uint16_t reserved, const std::string & dirId, int clntCnt);
    static int decode_SVRASKCLNT_REGET_FULLDIR(char *pOutBuf, int len, uint16_t &reserved, std::string & dirId, int &clntCnt);
    
};


#endif 