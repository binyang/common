
#include "ftpproto.h"
#include "packetheader.h"
#include "packer.h"
#include <util/asiofunc.h>
#include "stdinl.h"
#include "proto.h"
#include <memory.h>
#include <iostream>
#include <assert.h>
#include <sys/stat.h>


FtpCtx::FtpCtx(uint8_t ctxType_, uint32_t syncTm_, uint32_t totalSize_, uint32_t fileCnt_,  std::string fsName_)
    : ctxType(ctxType_)
    , syncTm(syncTm_)
    , totalSize(totalSize_)
    , fileCnt(fileCnt_)
    , fsName(fsName_)
{  
}

FtpCtx::FtpCtx(const FtpCtx & rhs)
    : ctxType(rhs.ctxType)
    , syncTm(rhs.syncTm)
    , totalSize(rhs.totalSize)
    , fileCnt(rhs.fileCnt)
    , fsName(rhs.fsName)
{    
}

bool FtpCtx::isDirCtx()
{
    return ctxType == CTX_GET_ALLDIR ||
           ctxType == CTX_GET_INCDIR ||
           ctxType == CTX_PUT_ALLDIR ||
           ctxType == CTX_PUT_INCDIR ;
}

int FtpProto::encode_SVR_ACK_PUT(char *pOutBuf, int len, uint16_t reserved, int errorCode)
{
    return Proto::encode_Int_Body(pOutBuf, len,RT_FILE_FTP, SVR_ACK_PUT, reserved, errorCode);
}

int FtpProto::encode_CLNT_PUT_FILE(ByteBuf_t &byteBuf, uint16_t reserved, 
                                   const std::string &fileName, const std::string &clntMd5)
{
    return Proto::encode_2Str_Body(byteBuf, RT_FILE_FTP, CLNT_PUT_FILE, reserved, fileName, clntMd5 );
//     StrVector_t v;
//     v.push_back(fileName);
//     v.push_back(clntMd5);
//     
//     return FtpProto::encode_FTP_FILE_LST(byteBuf, CLNT_PUT_FILE, reserved, v);    
}

int FtpProto::decode_CLNT_PUT_FILE(char *pOutBuf, int len, std::string &fileName, std::string &clntMd5)
{
    return Proto::decode_2Str_Body(pOutBuf, len, fileName, clntMd5);
}

int FtpProto::encode_SVR_CMP_PUTFILE(char *pOutBuf, int len, uint16_t reserved,  bool bChanged)
{
    return Proto::encode_Int_Body (pOutBuf, len, RT_FILE_FTP, SVR_CMP_PUTFILE, reserved, (int)bChanged);
}

int FtpProto::encode_CLNT_PUT_REQ(char *pOutBuf, int len, uint16_t reserved, const FtpCtx &ftpCtx)
{
    return FtpProto::encode_FILE_CTX(
        pOutBuf, len, CLNT_PUT_REQ, reserved, ftpCtx);
}

int FtpProto::encode_CLNT_GET_REQ(char *pOutBuf, int len, uint16_t reserved, const FtpCtx &ftpCtx)
{
    return FtpProto::encode_FILE_CTX(
        pOutBuf, len, CLNT_GET_REQ, reserved, ftpCtx);
}

int FtpProto::encode_CLNT_GET_FILE(ByteBuf_t &byteBuf, uint16_t reserved, 
                                   const std::string &fileName, const std::string &clntMd5)
{
    return Proto::encode_2Str_Body(byteBuf, RT_FILE_FTP, CLNT_GET_FILE, reserved, fileName, clntMd5);
//     StrVector_t v;
//     v.push_back(fileName);
//     v.push_back(clntMd5);
//     
//     return FtpProto::encode_FTP_FILE_LST(byteBuf, CLNT_GET_FILE, reserved, v); 
}

int FtpProto::decode_CLNT_GET_FILE(char *pOutBuf, int len, std::string &fileName, std::string &clntMd5)
{
    return Proto::decode_2Str_Body(pOutBuf, len, fileName, clntMd5);
}

int FtpProto::encode_SVR_ACK_GET(ByteBuf_t &byteBuf, uint16_t reserved, const StrVector_t &fileList)
{
    return Proto::encode_StrVector_Body(
        byteBuf, RT_FILE_FTP, SVR_ACK_GET, reserved, fileList);    
}

int FtpProto::encode_FTP_FILE_DATA(char *pOutBuf, int len, int bodyLen, uint16_t reserved)
{
    int tot = PacketHeader::size();
    tot  += bodyLen;
    
    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(RT_FILE_FTP, FTP_FILE_DATA, reserved, tot);
    return tot;
}

int FtpProto::encode_FTP_EOF(char *pOutBuf, int size, uint16_t reserved, uint32_t tm, const std::string &md5)
{
    int tot = PacketHeader::size();
    char *ptr = pOutBuf + PacketHeader::size();

    int len = Packer::pack(ptr, size, tm); 
    tot += len;
    ptr += len;
    
    int strLen = (int)md5.size();
    len = Packer::packLenStr(ptr, size, md5.c_str(), strLen); 
    tot += len;
    ptr += len;

    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(RT_FILE_FTP, FTP_EOF, reserved, tot);
    return tot;    
    //return Proto::encode_Str_Body(pOutBuf, len, RT_FILE_FTP, FTP_EOF, reserved, md5);
}

int FtpProto::decode_FTP_EOF(char *pOutBuf, int size, uint32_t &tm, std::string &md5)
{
    if ( size < 0 || 
        size > FTPBUF_DEFSIZE )
        return -1;
        
    char * ptr = pOutBuf;
    int len = Packer::unpack(ptr, tm);  //tm
    size -= len;
    ptr  += len;
    
    int strLen = 0;
    char pBuf[256];
    len = Packer::unpackLenStr(ptr, pBuf, strLen);
    pBuf[strLen] = '\0';
    md5 = pBuf;
    return 0;    
    
    //return Proto::decode_Str_Body(pOutBuf, len, md5);
}

///////////////////////////////////////common //////////////////////////////////////////
int FtpProto::encode_FILE_CTX(char *pOutBuf, int size, uint8_t sType, 
                                       uint16_t reserved, const FtpCtx &ftpCtx)
{
    int tot = PacketHeader::size();
    char *ptr = pOutBuf + PacketHeader::size();

    int len = Packer::pack(ptr, size, ftpCtx.ctxType); bool isDirCtx(const FtpCtx &);
    tot += len;
    ptr += len;
    
    len = Packer::pack(ptr, size, ftpCtx.syncTm);
    tot += len;
    ptr += len;
    
    len = Packer::pack(ptr, size, ftpCtx.totalSize);
    tot += len;
    ptr += len;
    
    len = Packer::pack(ptr, size, ftpCtx.fileCnt);
    tot += len;
    ptr += len;
    
    int strLen = (int)ftpCtx.fsName.size();
    len = Packer::packLenStr (ptr, size, ftpCtx.fsName.c_str(), strLen);
    tot += len;
    ptr += len;
    
    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(RT_FILE_FTP, sType, reserved, tot);
    return tot;
}

int FtpProto::decode_FILE_CTX(char *pOutBuf, int size, FtpCtx &ftpCtx)
{
    if ( size < 0 || 
        size > FTPBUF_DEFSIZE )
        return -1;
    
    char * ptr = pOutBuf;
    int len = Packer::unpack(ptr, ftpCtx.ctxType);
    size -= len;
    ptr  += len;

    len = Packer::unpack(ptr, ftpCtx.syncTm);
    size -= len;
    ptr  += len;
    
    len = Packer::unpack(ptr, ftpCtx.totalSize);
    size -= len;
    ptr  += len;
    
    len = Packer::unpack(ptr, ftpCtx.fileCnt);
    size -= len;
    ptr  += len;
    
    int strLen = 0;
    char pBuf[256];
    len = Packer::unpackLenStr(ptr, pBuf,strLen);
    size -= len;
    ptr  += len;
    pBuf[strLen] = '\0';
    ftpCtx.fsName = pBuf;
    
    return 0;
}

int FtpProto::encode_REQ_PUT_BLNKDIRS(ByteBuf_t &byteBuf, uint16_t reserved, StrVector_t &bigList)
{
    return Proto::encode_StrVector_Big_Body(byteBuf, RT_FILE_FTP, REQ_PUT_BLNKDIRS, reserved, bigList);
}

int FtpProto::encode_SVR_RESP_FLST(ByteBuf_t &byteBuf, uint16_t reserved, StrVector_t& bigList)
{
     return Proto::encode_StrVector_Big_Body(byteBuf, RT_FILE_FTP,  SVR_RESP_FLST, reserved, bigList);
}

int FtpProto::encode_SVR_RESP_FLST_DONE(char *pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_FILE_FTP, SVR_RESP_FLST_DONE, reserved);
}

int FtpProto::encode_CLNT_PUTDIR_DONE(char *pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_FILE_FTP, CLNT_PUTDIR_DONE, reserved);
}

