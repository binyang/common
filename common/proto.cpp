
#include "proto.h"
#include "packetheader.h"
#include "packer.h"

#include <memory.h>
#include <iostream>
#include <assert.h>
#include <sys/stat.h>

int Proto::encodeHeaderOnly(char *pOutBuf, int size, uint8_t mType, uint8_t sType, uint16_t reserved)
{
    if (size < PacketHeader::size() || 
        size > FTPBUF_DEFSIZE ||
        mType > 100 ||
        sType > 100 )
        return -1;
    int tot = PacketHeader::size();
    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(mType, sType, reserved, tot);
    return tot;
}


int Proto::encode_Int_Body(char *pOutBuf, int size, uint8_t mType, uint8_t sType, uint16_t reserved, int val)
{
    if (size < PacketHeader::size() || 
        size > FTPBUF_DEFSIZE ||
        mType > 100 ||
        sType > 100 )
        return -1;
    
    int tot     = PacketHeader::size();
    char *ptr   = pOutBuf + PacketHeader::size();
    int len     = Packer::pack(ptr, size, val); 
    tot += len;

    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(mType, sType, reserved, tot);
    return tot;
}

int Proto::encode_Str_Body(char *pOutBuf, int size, uint8_t mType, uint8_t sType, uint16_t reserved, const std::string &str)
{
    if (size < PacketHeader::size() || 
        size > FTPBUF_DEFSIZE ||
        mType > 100 ||
        sType > 100 ||
        str.size() == 0 ||
        str.size() > FTPBUF_DEFSIZE )
        return -1;    
    
    int tot = PacketHeader::size();
    char *ptr = pOutBuf + PacketHeader::size();

    int strLen = (int)str.size();
    int len = Packer::packLenStr(ptr, size, str.c_str(), strLen); 
    tot += len;
    ptr += len;

    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(mType, sType, reserved, tot);
    return tot;    
}

int Proto::decode_Str_Body(char *pOutBuf, int len, std::string &str)
{
    if ( len < 1 ||  len > FTPBUF_DEFSIZE)
        return -1;
    
    int strLen = 0;
    char pBuf[256];
    len = Packer::unpackLenStr(pOutBuf, pBuf, strLen);
    pBuf[strLen] = '\0';
    str = pBuf;
    return 0;
}

int Proto::encode_2Str_Body(ByteBuf_t &byteBuf, uint8_t mType, uint8_t sType, uint16_t reserved,
                                   const std::string &str1, const std::string &str2)
{
    if ( mType > 100 ||
        sType > 100 ||
        str1.size() == 0 ||
        str1.size() > FTPBUF_DEFSIZE  ||
        str2.size() == 0 ||
        str2.size() > FTPBUF_DEFSIZE )
        return -1;       
    
    StrVector_t v;
    v.push_back(str1);
    v.push_back(str2);
    
    return encode_StrVector_Body(byteBuf, mType, sType, reserved, v); 
}

int Proto::decode_2Str_Body(char *pOutBuf, int len, std::string &str1, std::string &str2)
{
    if ( len < 1 || len > FTPBUF_DEFSIZE)
        return -1;
    
    std::vector<std::string> v;
    if (decode_StrVector_Body(pOutBuf, len, v) < 0)
        return -1;
    if (v.size() != 2)
        return -1;
    str1 = v[0];
    str2  = v[1];
    return 0;
}

int Proto::encode_StrVector_Body(ByteBuf_t &byteBuf, uint8_t mType, uint8_t sType, 
                                  uint16_t reserved, const StrVector_t &fileLst)
{
    if ( mType > 100 ||
        sType > 100 )
        return -1;       
    
    int sz, len;
    char pBuf[512];
    
    byteBuf.clear();
    byteBuf.reserve(FTPBUF_DEFSIZE);
    byteBuf.insert(byteBuf.end(), pBuf, pBuf + PacketHeader::size()); // insert PacketHeader 
    PacketHeader *pHeader = (PacketHeader *)byteBuf.data();
    
    sz = (int)fileLst.size();
    if (sz == 0)
        return Proto::encodeHeaderOnly((char*)pHeader, PacketHeader::size(), mType, sType, reserved);
    
    len = Packer::pack(pBuf, sizeof(pBuf), sz);             //fileCnt
    byteBuf.insert(byteBuf.end(), pBuf, pBuf + len);    
    
    for( const std::string & f : fileLst)
    {
        sz = (int)f.size();                             //strlen
        len = Packer::packLenStr (pBuf, sizeof(pBuf), f.c_str(), sz);//char*
        byteBuf.insert(byteBuf.end(), pBuf, pBuf + len);
    }
    pHeader->buildHeader(mType, sType, reserved, (int)byteBuf.size());
    return (int)byteBuf.size();
}

int Proto::encode_StrVector_Big_Body(ByteBuf_t& byteBuf, uint8_t mType, uint8_t sType, uint16_t reserved, StrVector_t& longLst)
{
    if ( mType > 100 ||
        sType > 100 )
        return -1;       
    
    int sz, len;
    char pBuf[512];
    
    byteBuf.clear();
    byteBuf.reserve(FTPBUF_DEFSIZE);
    byteBuf.insert(byteBuf.end(), pBuf, pBuf + PacketHeader::size()); // insert PacketHeader 
    PacketHeader *pHeader = (PacketHeader *)byteBuf.data();
    
    sz = (int)longLst.size();
    if (sz == 0)
        return Proto::encodeHeaderOnly((char*)pHeader, PacketHeader::size(), mType, sType, reserved);
    
    len = Packer::pack(pBuf, sizeof(pBuf), 0);             //actual fileCnt is unknown yet,just hold place
    byteBuf.insert(byteBuf.end(), pBuf, pBuf + len);    
    int fileCnt = 0;
    while(longLst.size() > 0  && byteBuf.size() < (FTPBUF_DEFSIZE - 256) )
    {
        const std::string & f = longLst.back();
        std::cout << "size:" << longLst.size() << "; back file:" << f << std::endl;
        
        sz = (int)f.size();                             //strlen
        len = Packer::packLenStr (pBuf, sizeof(pBuf), f.c_str(), sz);//char*
        byteBuf.insert(byteBuf.end(), pBuf, pBuf + len);
        longLst.pop_back();
        fileCnt++;
    }
    
    pHeader->buildHeader(mType, sType, reserved, (int)byteBuf.size());
    *(int *)(byteBuf.data() + sizeof(PacketHeader)) = fileCnt;  //placehoder is set by actual fileCnt
    return (int)byteBuf.size();
}


int Proto::decode_StrVector_Body(char *pOutBuf, int size, StrVector_t &fileLst)
{
    if ( size < 1 ||  size > FTPBUF_DEFSIZE)
        return -1;
    
    int cnt, strLen;
    char pBuf[512];
    
    char * ptr = pOutBuf;
    int len = Packer::unpack(ptr, cnt);         //fileCnt
    size -= len;
    ptr  += len;
    
    for(int i = 0 ; i < cnt; ++i)
    {
        len = Packer::unpackLenStr (ptr, pBuf, strLen);
        size -= len;
        ptr  += len;
        
        pBuf[strLen] = '\0';
        fileLst.push_back(pBuf);
    }
    return 0;
}


