#ifndef __PROTO__H
#define __PROTO__H

#include "stdinl.h"
#include <string>

class Proto
{
public:
    static int encodeHeaderOnly(char *pOutBuf, int size, uint8_t mType, uint8_t sType, uint16_t reserved);
    static int encode_Int_Body(char *pOutBuf, int size, uint8_t mType, uint8_t sType, uint16_t reserved, int val);

    static int encode_Str_Body(char *pOutBuf, int size, uint8_t mType, uint8_t sType, uint16_t reserved, const std::string &str);
    static int decode_Str_Body(char *pOutBuf, int len, std::string &str);

    static int encode_2Str_Body(ByteBuf_t &byteBuf, uint8_t mType, uint8_t sType, uint16_t reserved, const std::string &str1, const std::string &str2);
    static int decode_2Str_Body(char *pOutBuf, int len, std::string &str1, std::string &str2);

    static int encode_StrVector_Body(ByteBuf_t &byteBuf, uint8_t mType, uint8_t sType, uint16_t reserved, const StrVector_t &fileLst);
    static int encode_StrVector_Big_Body(ByteBuf_t &byteBuf, uint8_t mType, uint8_t sType, uint16_t reserved, StrVector_t &longLst);
    
    static int decode_StrVector_Body(char *pOutBuf, int size, StrVector_t &fileLst);
};


#endif 