
#include "authproto.h"
#include "proto.h"
#include "common/packetheader.h"
#include "common/packer.h"
#include <util/logger.h>
#include <util/asiofunc.h>

#include <memory.h>
#include <iostream>
#include <assert.h>

int AuthProto::encode_AUTH_SVR_ACCEPT(char *pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_AUTH, AUTH_SVR_ACCEPT, reserved);
//     int tot = PacketHeader::size();
//     PacketHeader *pHeader = (PacketHeader *)pOutBuf;
//     pHeader->buildHeader(RT_AUTH, AUTH_SVR_ACCEPT, reserved, tot);
//     return tot;    
}

int AuthProto::encode_AUTH_SVR_REJECT(char *pOutBuf, int len, uint16_t reserved)
{
    return Proto::encodeHeaderOnly(pOutBuf, len, RT_AUTH, AUTH_SVR_REJECT, reserved);
    
//     int tot = PacketHeader::size();
//     PacketHeader *pHeader = (PacketHeader *)pOutBuf;
//     pHeader->buildHeader(RT_AUTH, AUTH_SVR_REJECT, reserved, tot);
//     return tot;
}
    
int AuthProto::encode_AUTH_CLNT_REQ(char *pOutBuf, int size, uint16_t reserved,
                                    const Crenditenal_t & crend)
{
    int tot = PacketHeader::size();
    char *ptr = pOutBuf + PacketHeader::size();

    int len = Packer::pack(ptr, size, crend.linkType);
    tot += len;
    ptr += len;
    
    len = Packer::pack(ptr, size, crend.mode);
    tot += len;
    ptr += len;    

    len = Packer::pack(ptr, size, crend.userId);
    tot += len;
    ptr += len;    
    
    len = Packer::packLenStr(ptr, size, crend.uuid.c_str(), (int)crend.uuid.length());
    tot += len;
    ptr += len;
    
    len = Packer::packLenStr(ptr, size, crend.user.c_str(), (int)crend.user.length());
    tot += len;
    ptr += len;
    
    len = Packer::packLenStr(ptr, size, crend.passwd.c_str(), (int)crend.passwd.length());
    tot += len;
    ptr += len;    
    
    PacketHeader *pHeader = (PacketHeader *)pOutBuf;
    pHeader->buildHeader(RT_AUTH, AUTH_CLNT_REQ, reserved, tot);
    return tot;
}

int AuthProto::decode_AUTH_CLNT_REQ(char *pOutBuf, int size, Crenditenal_t & crend)
{
    if ( size < 0 || 
        size > FTPBUF_DEFSIZE )
        return -1;
    
    char * ptr = pOutBuf;
    int strLen =0;
    char pBuf[256];

    int len = Packer::unpack(ptr, crend.linkType);
    ptr  += len;
    
    len = Packer::unpack(ptr, crend.mode);
    ptr  += len;

    len = Packer::unpack(ptr, crend.userId);
    ptr  += len;
    
    len = Packer::unpackLenStr(ptr, pBuf, strLen);
    ptr  += len;
    pBuf[strLen] = '\0';
    crend.uuid = pBuf;

    len = Packer::unpackLenStr(ptr, pBuf, strLen);
    ptr  += len;
    pBuf[strLen] = '\0';
    crend.user = pBuf;    

    len = Packer::unpackLenStr(ptr, pBuf, strLen);
    ptr  += len;
    pBuf[strLen] = '\0';
    crend.passwd = pBuf;        
    
    return 0;    
}
