#ifndef __FSFUNC_H
#define __FSFUNC_H

#include "stdinl.h"
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <string>
#include <vector>
#include <set>
#include <fstream>
class basic_fstream;
namespace fsfunc 
{
    int getDirVolume(const std::string & dirName, uint32_t *pCnt, uint32_t *pTotSz, StrVector_t * pFlist);
    int getDirFtpInfo(const std::string & docroot, const std::string & dirName, uint32_t cutoffTm,
                      uint32_t *pTotSz, StrVector_t * pFlist);
    
    int splitStr(const std::string &s, char delim, std::vector<std::string> &retV);
    
    int createMissingDirs(const std::string & docroot, const StrVector_t &flst);
    int getLongestPaths(const std::string &docroot, const std::string &dirName, StrVector_t & retLst);
    int expandLongestPaths(const StrVector_t & longestPath, int preffixLen, StrVector_t & allDirs);
    void delEmptyDirs(const std::string & dir);
    int remove(const std::string &docroot, const StrVector_t &fileLst);
    int getMapFilesOfDir(const std::string &docroot, const std::string &dirName, StrVector_t &fileLst);
    int getAllDirs(const std::string docRoot, const std::string &dirName, StrVector_t &fileLst);
    bool dirHasChild(const char *pDir);    
    
    bool isBitOn(uint16_t reserved, uint16_t bitmask);
    void setBitOn(uint16_t &reserved, uint16_t bitmask);
    void setBitOff(uint16_t &reserved, uint16_t bitmask);
    
    std::string md5sum(const std::string &fileName);
    int  readFileAll(const std::string &fileName,  std::string& outStr);
    int  writeFileAll(const std::string& fileName, const std::string &inStr);
    int  writeFileAll(const std::string& fileName, const char *pBuf, int len);
    //int  replaceTmplFile(const std::string &fileName, const std::unordered_map<std::string, std::string> & replace, std::string& outStr);
    
    
    //server append and merge the deleted file to log. used to push the list to client.
    int  logDelFiles(const std::string&docRoot, const StrVector_t & fl);

    //for each line in txt file
    void foreach_txtline(const std::string& fileName, cb_fn2 cbFn, void *args);
    
    //read string from pos to a deliminator
    int substrByDeliminator(const std::string& inStr, int pos, uint8_t deliminator, std::string & outStr);

    //read string from pos to a deliminator
    int substrByDeliminator(const std::string& inStr, int pos, uint8_t deliminator, std::string & str1, std::string & str2);
    
    
    //map local abs path to svr Id path 
    int mapDir2Id(const Dir2IdArr_t &dir2IdArr, 
                const std::string &localFs, std::string &mapFs);
    
    //map svr Id path to local abs path
    int mapId2Dir(const Dir2IdArr_t &dir2IdArr, 
                const std::string &mapFs, std::string &localFs);    
    
    int mapDir2Id(const Dir2IdArr_t &dir2IdArr, 
                const StrVector_t &localFs, StrVector_t &mapFs);
    int mapId2Dir(const Dir2IdArr_t &dir2IdArr, 
                const StrVector_t &mapFs, StrVector_t &localFs);    
    
    void replaceOne(std::string& subject, const std::string& oldStr,
                          const std::string& newStr);
    void mapSlash2Local(std::string &fsName);
    void mapSlash2Svr(std::string &fsName);
    //lhs has, but rhs does not    
    StrVector_t set1Diff(const StrVector_t &lhs, const StrVector_t &rhs);
    
    class FStream : public std::basic_fstream<char> {
    public:
        void open (const char* filename,
                ios_base::openmode mode = ios_base::in | ios_base::out);
        const std::string &getFileName() const
        {   return m_fileName;      }
        
        bool isTmpFile(const std::string & prefix, std::string &orig) const;
    private:
        std::string m_fileName;        
    };    
    
    
}
#endif